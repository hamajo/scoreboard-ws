from django.db import models
from datetime import datetime


class Player(models.Model):
    name = models.CharField(max_length=12)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Game(models.Model):
    name = models.CharField(max_length=100)
    participations = models.ManyToManyField(Player, through='Participation', through_fields=('game', 'player'))

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Participation(models.Model):
    win = models.BooleanField(default=False)
    participation_date = models.DateTimeField(default=datetime.now(), blank=True)
    game = models.ForeignKey(Game, related_name='participants')
    player = models.ForeignKey(Player, related_name='played_games')

    @classmethod
    def score_for_player(cls, player):
        return len(Participation.objects.filter(player=player, win=True))

    def __str__(self):
        if self.win:
            return "[" + self.game.name + "] " + self.player.name + " (VICTOIRE)"
        else:
            return "[" + self.game.name + "] " + self.player.name + " (DEFAITE)"

    def __unicode__(self):
        if self.win:
            return "[" + self.game.name + "] " + self.player.name + " (VICTOIRE)"
        else:
            return "[" + self.game.name + "] " + self.player.name + " (DEFAITE)"

