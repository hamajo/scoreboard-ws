from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

import views

urlpatterns = patterns(
    '',
    url(r'^$', views.scoreboard),
    url(r'^best/$', views.scoreboard_best),
    url(r'^worst/$', views.scoreboard_worst),

    url(r'^api/$', views.api_root),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', auth_views.login),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    url(r'^api/games/(?P<pk>[0-9]+)/win/(?P<pk_player>[0-9]+)/$', views.add_win),
    url(r'^api/games/(?P<pk>[0-9]+)/play/(?P<pk_player>[0-9]+)/$', views.add_participation),

    url(r'^api/games/$', views.GameList.as_view(), name="games-list"),
    url(r'^api/games/(?P<pk>[0-9]+)/$', views.GameDetail.as_view(), name="games-details"),
    url(r'^api/players/$', views.PlayerList.as_view(), name="players-list"),
    url(r'^api/players/(?P<pk>[0-9]+)/$', views.PlayerDetail.as_view(), name="players-details"),
    url(r'^api/participations/$', views.ParticipationList.as_view(), name="participations-list"),
    url(r'^api/participations/(?P<pk>[0-9]+)/$', views.ParticipationDetail.as_view(), name="participations-details"),
)
