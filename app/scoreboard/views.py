# -*- coding: utf-8 -*-
import json

from django.http.response import HttpResponse
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.reverse import reverse

from models import Game, Player, Participation
from serializers import GameSerializer, PlayerSerializer, ParticipationSerializer


def scoreboard(request):
    try:
        last_win = Participation.objects.filter(win=True).order_by('-participation_date')[0]
        last_winner = last_win.player.name
        game = last_win.game.name
    except IndexError:
        last_winner = None
        game = None
    return render(request, 'scoreboard/index.html', {'last_winner': last_winner, 'game': game})


def scoreboard_best(request):
    players = sorted(Player.objects.all(), key=lambda player: Participation.score_for_player(player), reverse=True)[:4]
    scores = []

    for p in players:
        scores.append({'name': p.name, 'score': Participation.score_for_player(Player.objects.get(name=p))})

    return render(request, 'scoreboard/best.html', {'scores': scores})


def scoreboard_worst(request):
    players = sorted(Player.objects.all(), key=lambda player: Participation.score_for_player(player))[:4]
    scores = []

    for p in players:
        scores.append({'name': p.name, 'score': Participation.score_for_player(Player.objects.get(name=p))})

    return render(request, 'scoreboard/worst.html', {'scores': scores})


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'games': reverse('games-list', request=request, format=format),
        'players': reverse('players-list', request=request, format=format),
        'participations': reverse('participations-list', request=request, format=format)
    })


@api_view(('POST',))
def add_win(request, pk, pk_player):
    try:
        game = Game.objects.get(pk=pk)
        player = Player.objects.get(pk=pk_player)

        participation = Participation.objects.filter(game=game, player=player).latest('participation_date')
        participation.win = True
        participation.save()

        data = {'result': True}
    except Game.DoesNotExist:
        data = {'result': False, 'error': u'Aucun jeu trouvé pour l\'id donnée'}
    except Player.DoesNotExist:
        data = {'result': False, 'error': u'Aucun joueur trouvé pour l\'id donnée'}
    except Participation.DoesNotExist:
        data = {'result': False, 'error': u'Aucune partie trouvée pour le jeu et le joueur donnés'}

    return HttpResponse(json.dumps(data), content_type='application/json')


@api_view(('POST',))
def add_participation(request, pk, pk_player):
    try:
        game = Game.objects.get(pk=pk)
        player = Player.objects.get(pk=pk_player)

        participation = Participation(
            game=game,
            player=player,
        )

        participation.save()

        data = {'result': True}
    except Game.DoesNotExist:
        data = {'result': False, 'error': 'Aucun jeu trouvé pour l\'id donnée'}
    except Player.DoesNotExist:
        data = {'result': False, 'error': 'Aucun joueur trouvé pour l\'id donnée'}

    return HttpResponse(json.dumps(data), content_type='application/json')


class GameList(ListAPIView):
    """List all games"""
    queryset = Game.objects.all()
    serializer_class = GameSerializer


class GameDetail(RetrieveAPIView):
    """Retrieve a game"""
    queryset = Game.objects.all()
    serializer_class = GameSerializer


class PlayerList(ListAPIView):
    """List all players"""
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer


class PlayerDetail(RetrieveAPIView):
    """Retrieve a player"""
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer


class ParticipationList(ListAPIView):
    """List all participations"""
    queryset = Participation.objects.all()
    serializer_class = ParticipationSerializer


class ParticipationDetail(RetrieveAPIView):
    """Retrieve a participation"""
    queryset = Participation.objects.all()
    serializer_class = ParticipationSerializer
