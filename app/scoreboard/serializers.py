from rest_framework import serializers
from models import *


class PlayerSerializer(serializers.ModelSerializer):
    depth = 2

    class Meta:
        model = Player
        fields = ('id', 'name', 'played_games')


class ParticipationSerializer(serializers.ModelSerializer):
    depth = 2

    class Meta:
        model = Participation
        fields = ('player', 'game', 'win')


class GameSerializer(serializers.ModelSerializer):
    depth = 2

    class Meta:
        model = Game
        fields = ('id', 'name', 'participations')