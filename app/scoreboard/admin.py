from django.contrib import admin
from models import Player, Participation, Game

admin.autodiscover()
admin.site.register(Player)
admin.site.register(Participation)
admin.site.register(Game)
